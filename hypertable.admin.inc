<?php
/**
 * @file
 * For admin form for the hypertable module.
 */

/**
 * System setting form for hypertable.
 *
 * @return array
 *   Admin form items to save.
 */
function hypertable_admin_form() {
  $form = array();
  $form['hypertable_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host'),
    '#default_value' => variable_get('hypertable_host'),
    '#size' => 20,
  );
  $form['hypertable_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => variable_get('hypertable_port'),
    '#size' => 10,
  );
  $form['hypertable_framesize'] = array(
    '#type' => 'textfield',
    '#title' => t('Framesize'),
    '#default_value' => variable_get('hypertable_framesize'),
    '#size' => 2,
  );
  $form['hypertable_thrift_root'] = array(
    '#type' => 'textfield',
    '#title' => t('Thrift'),
    '#default_value' => variable_get('hypertable_thrift_root'),
    '#size' => 100,
    '#description' => t('Select the location your thirft library is located.'),
  );

  return system_settings_form($form);
}
