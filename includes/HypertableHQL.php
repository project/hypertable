<?php
/**
 * @file
 * Hypertable module class for extending Thrift Client Library.
 */

// Include Hypertable Thrift Library.
libraries_load('hypertable');

/**
 * HT Exception class.
 */
class HypertableHQLException extends Exception {
  /**
   * Exception object.
   */
  protected $trace;

  /**
   * Constructor.
   */
  public function __construct(Exception $trace) {
    $this->trace = $trace;
    $this->message = $trace->getMessage();
  }

  /**
   * Return HypertableHQL message.
   */
  public function getError() {
    return $this->trace;
  }
}

/**
 * HypertableHQL base class. 
 */
class HypertableHQL extends Hypertable_ThriftClient {
  /**
   * HypertableModify instance.
   *
   * @var string
   *   Set instance to NULL.
   */
  static protected $instance = NULL;

  /**
   * Implement HypertableHQL construct.
   */
  public function __construct() {
    try {
      parent::__construct(HYPERTABLE_HOST, HYPERTABLE_PORT);
    }
    catch (TTransportException $e) {
      throw new HypertableHQLException($e);
    }
    catch (TException $e) {
      throw new HypertableHQLException($e);
    }
  }

  /**
   * Implement HypertableAPI destruct.
   */
  public function __destruct() {}

  /**
   * Implement check if namespace exists.
   *
   * @param string $ns
   *   Namespace to chect it exists.
   *
   * @return object
   *   Namespace object returned.
   */
  public function existsNamespace($ns) {
    $this->result = $this->namespace_exists($ns);

    return $this->result;
  }

  /**
   * Implement creation of namespace.
   *
   * @param string $ns
   *   Namespace to create.
   *
   * @return object
   *   response object
   */
  public function createNamespace($ns) {
    $this->result = $this->namespace_create($ns);

    return $this;
  }

  /**
   * Implement droping namespace.
   *
   * @param string $ns
   *   Namespace to drop.
   *
   * @return object
   *   response object
   */
  public function dropNamespace($ns) {
    $this->result = $this->namespace_drop($ns, $if_exists = '1');

    return $this;
  }

  /**
   * Implement listing of namespaces.
   *
   * @param string $ns
   *   Namespace to list.
   *
   * @return object
   *   \HypertableAPI object
   */
  public function listNamespace($ns) {
    $namespace = $this->namespace_open($ns);
    $this->result = $this->namespace_get_listing($namespace);
    $this->namespace_close($namespace);

    return $this;
  }

  /**
   * Implement creation of table.
   *
   * @param string $ns
   *   Namespace to create table in.
   *
   * @param object $table_schema
   *   object with table and column schema.
   *
   * @return object
   *   \HypertableAPI
   */
  public function createTable($ns, $table_schema) {
    $namespace = $this->namespace_open($ns);
    $this->table_schema = $table_schema;

    // Format fields for query.
    $fields = implode(",", $table_schema->fields);
    $this->result = $this->hql_query($namespace, 'create table ' . $table_schema->name . ' (' . $fields . ')');
    $this->namespace_close($namespace);

    return $this;
  }

  /**
   * Implement drop table.
   *
   * @param string $ns
   *   Namespace to open.
   *
   * @param string $table
   *   Table to drop.
   */
  public function dropTable($ns, $table) {
    $namespace = $this->namespace_open($ns);
    $this->drop = $this->hql_query($namespace, 'DROP TABLE IF EXISTS ' . $table . '');
    $this->namespace_close($namespace);
  }

  /**
   * Implement return of tables in specified namespace.
   *
   * @param string $ns
   *   Namespace to list tables in.
   *
   * @return object
   *   \HypertableAPI object.
   */
  public function showTables($ns) {
    $namespace = $this->namespace_open($ns);
    $this->result = $this->hql_query($namespace, "SHOW TABLES");
    $this->namespace_close($namespace);

    return $this;
  }

  /**
   * Implement table description.
   *
   * @param string $ns
   *   Namespace to open.
   *
   * @param string $table
   *   Table to describe.
   *
   * @return object
   *   \HypertableAPI
   */
  public function describeTable($ns, $table) {
    $namespace = $this->namespace_open($ns);
    $this->result = $this->hql_query($namespace, "DESCRIBE TABLE WITH IDS " . $table);
    $this->namespace_close($namespace);

    return $this;
  }

  /**
   * Implement return TRUE if table exists or FALSE.
   *
   * @param string $ns
   *   Namespace to open.
   *
   * @param string $table
   *   Table to check if exists.
   *
   * @return string
   *   \HypertableAPI
   */
  public function existsTable($ns, $table) {
    $namespace = $this->namespace_open($ns);
    $this->result = $this->hql_query($namespace, "EXISTS TABLE " . $table);
    $this->namespace_close($namespace);

    return $this->results[0];
  }

  /**
   * Implement renaming of table.
   *
   * @param string $ns
   *   Namespace to open.
   *
   * @param string $table
   *   Table to rename.
   *
   * @param string $new_table
   *   New table name.
   *
   * @return object
   *   \HypertableAPI object
   */
  public function renameTable($ns, $table, $new_table) {
    $namespace = $this->namespace_open($ns);
    $this->result = $this->hql_query($namespace, "RENAME TABLE " . $table . " TO " . $new_table);
    $this->namespace_close($namespace);

    return $this;
  }

  /**
   * Dumps table into current Hypertable directory.
   *
   * @param string $ns
   *   Namespace to open.
   *
   * @param string $table
   *   Table to perform dump on.
   *
   * @return object
   *   \HypertableAPI object.
   */
  public function dumpTable($ns, $table) {
    $namespace = $this->namespace_open($ns);
    $this->result = $this->hql_query($namespace, "DUMP TABLE " . $table . " INTO FILE '" . $table . ".tsv.gz'");
    $this->namespace_close($namespace);

    return $this;
  }

  /**
   * Implement return of table schema.
   *
   * @param string $ns
   *   Namespace to open.
   *
   * @param array $table
   *   Table to get schema from.
   *
   * @return object
   *   \HypertableAPI
   */
  public function tableGetSchema($ns, $table) {
    $namespace = $this->namespace_open($ns);
    $this->result = $this->table_get_schema($namespace, $table);
    $this->namespace_close($namespace);

    return $this;
  }

  /**
   * Implements alter table.
   */
  public function alterTable() {

  }

  /**
   * Implements load data from file.
   */
  public function loadDataInfile() {

  }

  /**
   * Get instance of HypertableHQL.
   *
   * @return string
   *   Set instance of self.
   */
  static public function getInstance() {
    if (!self::$instance) {
      self::$instance = new HypertableHQL();
    }
    return self::$instance;
  }
}

class HypertableQuery extends HypertableHQL {
  /**
   * HypertableModify instance.
   *
   * @var string
   *   Set instance to NULL.
   */
  static protected $instance = NULL;

  /**
   * Set Namespace.
   *
   * @param string $ns
   *   Set namespace to operate on.
   *
   * @return string
   *   \HypertableQuery
   */
  public function setNS($ns) {
    $this->ns = $ns;
    return $this;
  }

  /**
   * Set Query.
   *
   * @param string $query 
   *   Set query to perform.
   *
   * @return string
   *   \HypertableQuery
   */
  public function setQuery($query) {
    $this->query = $query;
    return $this;
  }

  /**
   * Execute HypertableDelete.
   *
   * @return object
   *   \HypertbleDelete
   */
  public function execute() {
    $namespace = $this->namespace_open($this->ns);
    $this->delete = $this->hql_query($namespace, $this->query);
    $this->namespace_close($namespace);
    return $this;
  }

  /**
   * Get instance of HypertableQuery.
   *
   * @return string
   *   Set instance of self.
   */
  static public function getInstance() {
    if (!self::$instance) {
      self::$instance = new HypertableQuery();
    }
    return self::$instance;
  }
}

class HypertableSelect extends HypertableHQL {
  /**
   * HypertableModify instance.
   *
   * @var string
   *   Set instance to NULL.
   */
  static protected $instance = NULL;

  /**
   * The conditional object of the WHERE clause.
   *
   * @var SelectCondition 
   */
  protected $where;

  /**
   * The condition to filter revision.
   *
   * @var SelectCondition 
   */
  protected $revs;

  /**
   * The amount to limit the query.
   * 
   * @var int 
   */
  protected $limit;

  /**
   * The offst to return from the query.
   *
   * @var int 
   */
  protected $offset;

  /**
   * Set Namespace.
   *
   * @param string $ns
   *   Set namespace to operate on.
   *
   * @return string
   *   \HypertableSelect
   */
  public function setNS($ns) {
    $this->ns = $ns;
    return $this;
  }

  /**
   * Set Table.
   *
   * @param string $table
   *   Set table to operate on.
   *
   * @return string
   *   \HypertableSelect
   */
  public function setTable($table) {
    $this->table = ' FROM ' . $table;
    return $this;
  }

  /**
   * Set Column.
   *
   * @param array $column
   *   Set select and columns for HQL.
   *
   * @return string
   *   \HypertableSelect
   */
  public function setColumn($column) {
    $this->column = !empty($column) ? 'SELECT ' .  implode(", ", $column) : "*";
    return $this;
  }

  /**
   * Set Where.
   *
   * @param array $where
   *   Set where clause for HQL.
   *
   * @return string
   *   \HypertableSelect
   */
  public function setWhere($where) {
    $where['2'] = '"' . $where['2'] . '"';
    $where = implode(" ", $where);
    $this->where = !empty($where) ? 'WHERE ' . $where : '';
    return $this;
  }

  /**
   * Set Revs.
   *
   * @param string $revs
   *   Set revision number for HQL.
   *
   * @return string
   *   \HypertableSelect
   */
  public function setRevs($revs) {
    $this->revs = !empty($revs) ? 'REVS=' . $revs : '';
    return $this;
  }

  /**
   * Set Limit.
   *
   * @param string $limit
   *   Set limit count for HQL.
   *
   * @return string
   *   \HypertableSelect
   */
  public function setLimit($limit = NULL) {
    $this->limit = !empty($limit) ? 'LIMIT=' . $limit : '';
    return $this;
  }

  /**
   * Set Offset.
   *
   * @param string $offset
   *   Set offset for HQL.
   *
   * @return string
   *   \HypertableSelect
   */
  public function setOffset($offset) {
    $this->offset = !empty($offset) ? 'OFFSET=' . $offset : '';
    return $this;
  }

  /**
   * Set KeyOnly.
   *
   * @param string $keyonly
   *   Set keyonly for HQL.
   *
   * @return string
   *   \HypertableSelect
   */
  public function setKeyOnly($key = FALSE) {
    $this->keysonly = !empty($key) ? 'KEYS_ONLY' : '';
    return $this;
  }

  /**
   * Set Count for total.
   *
   * @return string
   *   HypertableSelect
   */
  public function setCount() {
    $namespace = $this->namespace_open($this->ns);
    $sql = $this->column . " " . $this->table . " " . $this->where . " " . $this->revs;
    $result = $this->hql_query_as_arrays($namespace,
       $sql . " KEYS_ONLY"
    );
    $this->namespace_close($namespace);
    $this->total = count($result->cells);
    return $this;
  }

  /**
   * Execute Query.
   *
   * @return object
   *   \HypertableSelect
   */
  public function execute() {
    $namespace = $this->namespace_open($this->ns);
    $sql = $this->column . "" . $this->table . " " . $this->where . " " . $this->revs
        . " " . $this->limit . " " . $this->offset . " " . $this->keysonly;
    $this->query = $this->hql_query($namespace, $sql);
    $this->namespace_close($namespace);
    return $this;
  }

  /**
   * Get instance of HypertableSelect.
   *
   * @return string
   *   Set instance of self.
   */
  static public function getInstance() {
    if (!self::$instance) {
      self::$instance = new HypertableSelect();
    }
    return self::$instance;
  }
}

class HypertableDelete extends HypertableHQL {
  /**
   * HypertableModify instance.
   *
   * @var string
   *   Set instance to NULL.
   */
  static protected $instance = NULL;

  /**
   * Set Namespace.
   *
   * @param string $ns
   *   Set namespace ot operate on.
   *
   * @return string
   *   \HypertableDelete
   */
  public function setNS($ns) {
    $this->ns = $ns;
    return $this;
  }

  /**
   * Set Table.
   *
   * @param string $table
   *   Set table to operate on.
   *
   * @return string
   *   \HypertableDelete
   */
  public function setTable($table) {
    $this->table = ' FROM ' . $table;
    return $this;
  }

  /**
   * Set Column.
   *
   * @param array $column
   *   Set select and columns for HQL.
   *
   * @return object
   *   \HypertableDelete
   */
  public function setColumn($column) {
    $this->column = !empty($column) ? "Delete " . implode(", ", $column) : "Delete *";
    return $this;
  }

  /**
   * Set Where.
   *
   * @param array $where
   *   Set where clause for HQL.
   *
   * @return string
   *   \HypertableDelete
   */
  public function setWhere($where) {
    $where['2'] = '"' . $where['2'] . '"';
    $where = implode(" ", $where);
    $this->where = !empty($where) ? 'WHERE ' . $where : '';
    return $this;
  }

  /**
   * Execute HypertableDelete.
   *
   * @return object
   *   \HypertbleDelete
   */
  public function execute() {
    $namespace = $this->namespace_open($this->ns);
    $sql = $this->column . " " . $this->table . " " . $this->where;
    $this->delete = $this->hql_query($namespace, $sql);
    $this->namespace_close($namespace);
    return $this;
  }

  /**
   * Get instance of HypertableDelete.
   *
   * @return string
   *   Set instance of self.
   */
  static public function getInstance() {
    if (!self::$instance) {
      self::$instance = new HypertableDelete();
    }
    return self::$instance;
  }
}

class HypertableInsert extends HypertableHQL {
  /**
   * HypertableModify instance.
   *
   * @var string
   *   Set instance to NULL.
   */
  static protected $instance = NULL;

  /**
   * Set Namespace.
   *
   * @param string $ns
   *   Set namespace to operate in.
   *
   * @return string
   *   \HypertableInsert
   */
  public function setNS($ns) {
    $this->ns = $ns;
    return $this;
  }

  /**
   * Set Table.
   *
   * @param string $table
   *   Set table to operate on.
   *
   * @return string
   *   \HypertableInsert
   */
  public function setTable($table) {
    $this->table = 'INSERT INTO ' . $table;
    return $this;
  }

  /**
   * Set Values.
   *
   * @param array $value
   *   Set values to insert.
   *
   * @return string
   *   \HypertableInsert
   */
  public function setValues($value) {
    $values = "";
    foreach ($value as $cells) {
      $values .= "('" . implode("', '", $cells) . "'),";
    }
    $this->insert = 'VALUES ' . trim($values, ',');
    return $this;
  }

  /**
   * Execute HypertableInsert.
   *
   * @return object
   *   \HypertableInsert
   */
  public function execute() {
    $namespace = $this->namespace_open($this->ns);
    $sql = $this->table . ' ' . $this->insert;
    $this->result = $this->hql_query($namespace, $sql);
    $this->namespace_close($namespace);
    return $this;
  }

  /**
   * Get instance of HypertableInsert.
   *
   * @return string
   *   Set instance of self.
   */
  static public function getInstance() {
    if (!self::$instance) {
      self::$instance = new HypertableInsert();
    }
    return self::$instance;
  }
}
