<?php
/**
 * @file
 * Hypertable module class for extending Thrift Client Library.
 */

// Include Hypertable Thrift Library.
libraries_load('hypertable');

/**
 * HT Exception class.
 */
class HypertableDriverException extends Exception {
  /**
   * Exception object.
   */
  protected $trace;

  /**
   * Constructor.
   */
  public function __construct(Exception $trace) {
    $this->trace = $trace;
    $this->message = $trace->getMessage();
  }

  /**
   * Return HypertableDriver message.
   */
  public function getError() {
    return $this->trace;
  }
}


/**
 * Hypertable Base class
 */
class HypertableDriver extends Hypertable_ThriftClient {
  /**
   * HypertableModify instance.
   *
   * @var string
   *   Set instance to NULL.
   */
  static protected $instance = NULL;

  /**
   * Hypertable namespace.
   *
   * @var string
   *    Set the namespace.
   */
  protected $ns;

  /**
   * Constructore for HypertableDriver.
   *
   * @throws HypertabeDriverException
   * @throws HypertableDriverException
   */
  public function __construct() {
    try {
      parent::__construct(HYPERTABLE_HOST, HYPERTABLE_PORT);
    }
    catch (TTransportException $e) {
      throw new HypertabeDriverException($e);
    }
    catch (TException $e) {
      throw new HypertableDriverException($e);
    }
  }

  /**
   * Get instance of HypertableDriver.
   *
   * @return string
   *   Set instance of self.
   */
  static public function getInstance() {
    if (!self::$instance) {
      self::$instance = new HypertableDriver();
    }
    return self::$instance;
  }

  /**
   * Destructor for HypertableDriver.
   */
  public function __destruct() {}
}

/**
 * HTDriverCondition
 */
class HypertableDriverCondition {

  const ROW_REGEXP = 0;

  const ROW_EQUAL = 1;

  const VALUE_REGEXP = 2;

  const VALUE_EQUAL = 3;

}

/**
 * Hypertable Select Driver
 */
final class HypertableDriverSelect extends HypertableDriver {
  /**
   * HypertableModify instance.
   *
   * @var string
   *   Set instance to NULL.
   */
  static protected $instance = NULL;

  /**
   * Columns to execute.
   *
   * @var array
   *   Define columns to operate on.
   */
  protected $columns = array();

  /**
   * Scanner to use.
   *
   * @var string
   *   Set the scanner to use.
   */
  protected $scanner;

  /**
   * Hypertable scanner argument.
   *
   * @var string
   *   Use to define arguments for scanner.
   */
  protected $args = NULL;

  /**
   * Column name (GROUP BY).
   *
   * @var string
   *   Use to define group.
   */
  protected $group = NULL;

  /**
   * Enable count of query.
   *
   * @var string
   *   Use to turn on counting.
   */
  protected $enableCount = FALSE;

  /**
   * Set Namespace.
   *
   * @param string $ns
   *   Set namespace to operate on.
   *
   * @return string
   *   \HypertableQuery
   */
  public function setNS($ns) {
    $this->ns = $ns;
    return $this;
  }

  /**
   * Table for scanner to operate on.
   *
   * @param string $table
   *   Define the table.
   *
   * @return object
   *   \HypertableDriverSelect
   */
  public function setTable($table) {
    $this->table = $table;
    return $this;
  }

  /**
   * Set scannspec arguments.
   *
   * @param string $args
   *   Sets the setsScanSpec in the object.
   *
   * @return object
   *   \HypertableDriverSelect
   */
  public function setScanSpec($args) {
    $this->args = $args;
    return $this;
  }

  /**
   * Set Target Column.
   *
   * @param string $col
   *   Set the column.
   *
   * @return object
   *   \HypertableDriverSelect
   */
  public function setColumn($col) {
    $this->columns[] = $col;
    $this->args['columns'][] = $col;
    return $this;
  }

  /**
   * Set Target columns.
   *
   * @param array $cols
   *   columns to merge.
   *
   * @return object
   *   \HypertableDriverSelect
   */
  public function setColumns(array $cols) {
    $this->columns = array_merge($this->columns, $cols);
    foreach ($cols as $col) {
      $this->args['columns'][] = $col;
    }
    return $this;
  }

  /**
   * Set condition (WHERE).
   *
   * @param string $type
   *   Type of condition.
   *
   * @param string $value
   *   Value to execute condition on.
   *
   * @return object
   *   \HypertableDriverSelect
   */
  public function condition($type, $value) {
    if ($type == HypertableDriverCondition::ROW_REGEXP) {
      $this->args['row_regexp'] = $value;
    }
    elseif ($type == HypertableDriverCondition::VALUE_REGEXP) {
      $this->args['value_regexp'] = $value;
    }
    elseif ($type == HypertableDriverCondition::ROW_EQUAL) {
      $interval = array(
        "start_row" => $value,
        "start_inclusive" => TRUE,
        "end_row" => $value,
        "end_inclusive" => TRUE,
      );
      $interval = new Hypertable_ThriftGen_RowInterval($interval);
      $this->args['row_intervals'][] = $interval;
    }
    elseif ($type == HypertableDriverCondition::VALUE_EQUAL) {
      $interval = array(
        "start_cell" => $value,
        "start_inclusive" => TRUE,
        "end_cell" => $value,
        "end_inclusive" => TRUE,
      );
      $interval = new Hypertable_ThriftGen_CellInterval($interval);
      $this->args['cell_intervals'] = $interval;
    }
    return $this;
  }

  /**
   * Set timestamp filter.
   *
   * @param int $ts
   *   Timestamp for argument.
   *
   * @param string $type
   *   Type of argument, < or >.
   *
   * @return object
   *   \HypertableDriverSelect
   */
  public function setTimestamp($ts, $type) {
    if ($type == ">") {
      $this->args['start_time'] = $ts;
    }
    elseif ($type == "<")
      $this->args['end_time'] = $ts;
    return $this;
  }

  /**
   * Set result limit.
   *
   * @param int $max
   *   Max row limit.
   *
   * @return object
   *   \HypertableDriverSelect
   */
  public function setLimit($max) {
    $this->args['row_limit'] = $max;
    return $this;
  }

  /**
   * Set cell offset.
   *
   * @param int $offset
   *   Offest for argument.
   *
   * @return object
   *   \HypertableDriverSelect
   */
  public function setCelloffset($offset) {
    $this->args['cell_offset'] = $offset;
    return $this;
  }

  /**
   * Set cell limit.
   *
   * @param int $max
   *   Cell limit to use for cells.
   *
   * @return object
   *   \HypertableDriverSelect
   */
  public function setCellLimit($max) {
    $this->args['cell_limit'] = $max;
    return $this;
  }

  /**
   * Set result offset.
   *
   * @param int $from
   *   Row offset to start with.
   *
   * @return object
   *   \HypertableDriverSelect
   */
  public function setOffset($from) {
    $this->args['row_offset'] = $from;
    return $this;
  }

  /**
   * Sort result by columns.
   *
   * @param object $data
   *   Object to sort on.
   *
   * @return object
   *   \stdClass
   */
  protected function sortColumns($data) {
    $result = array();
    foreach ($data as $cell) {
      $row = $cell->key->row;
      if (!isset($result[$row])) {
        $result[$row] = new stdClass();
        $result[$row]->row = $cell->key->row;
        foreach ($this->columns as $col) {
          $pos = strpos($col, ':');
          $qualifier = ($pos) ? drupal_substr($col, $pos + 1) : NULL;
          $col = !$pos ? $col : drupal_substr($col, 0, $pos);
          if (!$qualifier) {
            $result[$row]->{$col} = NULL;
          }
          elseif ($qualifier[0] != '/')
            $result[$row]->{$col}[$qualifier] = NULL;
        }
      }
      $result[$row]->{$cell->key->column_family}[$cell->key->column_qualifier] = $cell->value;
    }
    return $result;
  }

  /**
   * Set Key Only.
   *
   * @param string $value
   *   Value to set keys only.
   *
   * @return object
   *   \HypertableDriverSelect
   */
  public function setKeyOnly($value) {
    $this->args['keys_only'] = $value;
    return $this;
  }

  /**
   * Set Optimisation.
   *
   * @param string $val
   *   value for scan and fitler rows.
   *
   * @return object
   *   \HypertableDriverSelect
   */
  public function setOptimisation($val) {
    $this->args['scan_and_filter_rows'] = $val;
    return $this;
  }

  /**
   * Set Count.
   *
   * @return object
   *   \HypertableDriverSelect
   */
  public function count() {
    $this->args['keys_only'] = TRUE;
    $this->args['scan_and_filter_rows'] = TRUE;
    $this->enableCount = TRUE;
    return $this;
  }

  /**
   * Execute request.
   *
   * @return object
   *   Return results of ge_cells.
   *
   * @throws HypertableDriverException
   */
  public function execute() {
    $scanspec = NULL;
    $this->ns = $this->namespace_open($this->ns);
    if ($this->args) {
      $scanspec = new Hypertable_ThriftGen_ScanSpec($this->args);
    }
    try {
      $cells = $this->get_cells($this->ns, $this->table, $scanspec);
    }
    catch (Hypertable_ThriftGen_ClientException $e) {
      throw new HypertableDriverException($e);
    }

    $result = $this->sortColumns($cells);
    $result = $this->enableCount == TRUE ? count($result) : $result;
    $this->enableCount = FALSE;
    $this->args = NULL;
    $this->namespace_close($this->ns);
    return $result;
  }

  /**
   * Singleton get instance of HTDriverSelect.
   *
   * @return self
   *   Set instance of HypertableDriver Select.
   */
  static public function getInstance() {
    if (!self::$instance) {
      self::$instance = new HypertableDriverSelect();
    }
    return self::$instance;
  }
}

/**
 * Hypertable Update/Insert Driver.
 */
final class HypertableDriverUpdate extends HypertableDriver {
  /**
   * HypertableModify instance.
   *
   * @var string
   *   Set instance to NULL.
   */
  static protected $instance = NULL;

  /**
   * Hypertable mutator.
   */
  protected $mutator;

  /**
   * Hypertable cells.
   *
   * @var array
   *   Set cells as array.
   */
  protected $cells = array();

  /**
   * Set Namespace.
   *
   * @param string $ns
   *   Set namespace to operate on.
   *
   * @return string
   *   \HypertableQuery
   */
  public function setNS($ns) {
    $this->ns = $ns;
    return $this;
  }

  /**
   * Set table.
   *
   * @args = string
   */
  public function setTable($table) {
    $this->table = $table;
    return $this;
  }

  /**
   * Set cell.
   *
   * @param string $row
   *   Row to use during set cell.
   *
   * @param string $column
   *   colum to use during set cell.
   *
   * @param string $value
   *   value to use for set cell.
   *
   * @return object
   *   \HypertableDriverUpdate
   */
  public function setCell($row, $column, $value) {
    $key = new Hypertable_ThriftGen_Key(
      array(
        'row' => $row,
        'column_family' => $column,
        'flag' => 2,
      )
    );
    $this->cells[] = new Hypertable_ThriftGen_Cell(
      array(
        'key' => $key,
        'value' => $value,
      )
    );
    return $this;
  }

  /**
   * Set cell with qualifier.
   *
   * @param string $row
   *   Row to use during set cell.
   *
   * @param string $column
   *   Column to use during set cell with qualifier.
   *
   * @param string $qualifier
   *   Qualifier to use on set cell.
   *
   * @param string $value
   *   value to use during set sell.
   *
   * @return object
   *   \HypertableDriverUpdate
   */
  public function setCellWithQualifier($row, $column, $qualifier, $value) {
    $key = new Hypertable_ThriftGen_Key(
      array(
        'row' => $row,
        'column_family' => $column,
        'column_qualifier' => $qualifier,
        'flag' => 2,
      )
    );
    $this->cells[] = new Hypertable_ThriftGen_Cell(
      array(
        'key' => $key,
        'value' => $value,
      )
    );
    return $this;
  }

  /**
   * Execute Update/Insert requst.
   *
   * @return string
   *   results of insert.
   */
  public function execute() {
    $this->ns = $this->namespace_open($this->ns);
    $this->mutator = $this->mutator_open($this->ns, $this->table, NULL, 0);
    $insert = $this->mutator_set_cells($this->mutator, $this->cells);
    $this->mutator_flush($this->mutator);
    $this->mutator_close($this->mutator);
    $this->table = NULL;
    $this->cells = array();
    $this->namespace_close($this->ns);
    return $insert;
  }

  /**
   * Singleton get instance of HTDriverModify.
   *
   * @return string
   *   Instance of self.
   */
  static public function getInstance() {
    if (!self::$instance) {
      self::$instance = new HypertableDriverUpdate();
    }
    return self::$instance;
  }
}

/**
 * Hypertable Delete Driver
 */
final class HypertableDriverDelete extends HypertableDriver {
  /**
   * HypertableModify instance.
   *
   * @var type
   *   Set instance to NULL.
   */
  static protected $instance = NULL;

  /**
   * Hypertable mutator
   */
  protected $mutator;

  /**
   * Hypertable cells.
   *
   * @var array
   *   Set cells as array.
   */
  protected $cells = array();

  /**
   * Set Namespace.
   *
   * @param string $ns
   *   Set namespace to operate on.
   *
   * @return string
   *   \HypertableQuery
   */
  public function setNS($ns) {
    $this->ns = $ns;
    return $this;
  }

  /**
   * Set table.
   *
   * @args string
   */
  public function setTable($table) {
    $this->table = $table;
    return $this;
  }

  /**
   * Add row to delete.
   *
   * @param string $row
   *   Row to set.
   *
   * @return object
   *   \HypertableDriverDelete
   */
  public function setRow($row) {
    $this->cells[] = new Hypertable_ThriftGen_Cell(
      array(
        'key' => new Hypertable_ThriftGen_Key(
          array(
            'row' => $row,
            'flag' => 0,
          )
        ),
      )
    );
    return $this;
  }

  /**
   * Add cell to delete.
   *
   * @param string $row
   *   Define row of cell.
   *
   * @param string $column
   *   Define column of cell.
   *
   * @return array
   *   \HypertableDriverDelete
   */
  public function setCell($row, $column) {
    $this->cells[] = new Hypertable_ThriftGen_Cell(
      array(
        'key' => new Hypertable_ThriftGen_Key(
          array(
            'row' => $row,
            'column_family' => $column,
            'flag' => 1,
          )
        ),
      )
    );
    return $this;
  }

  /**
   * Add cell with qualifier to delete.
   *
   * @param string $row
   *   Set row.
   *
   * @param string $column
   *   Set column.
   *
   * @param string $qualifier
   *   Set qualifier.
   *
   * @return object
   *   \HypertableDriverDelete
   */
  public function setCellWithQualifier($row, $column, $qualifier) {
    $this->cells[] = new Hypertable_ThriftGen_Cell(
      array(
        'key' => new Hypertable_ThriftGen_Key(
          array(
            'column_qualifier' => $qualifier,
            'row' => $row,
            'column_family' => $column,
            'flag' => 1,
          )
        ),
      )
    );
    return $this;
  }

  /**
   * Delete all cells/rows.
   *
   * @return object
   *   Execute mutator.
   */
  public function execute() {
    $this->ns = $this->namespace_open($this->ns);
    $this->mutator = $this->mutator_open($this->ns, $this->table, 0, 0);
    $delete = $this->mutator_set_cells($this->mutator, $this->cells);
    $this->mutator_flush($this->mutator);
    $this->mutator_close($this->mutator);
    $this->table = NULL;
    $this->cells = array();
    $this->namespace_close($this->ns);
    return $delete;
  }

  /**
   * Singleton get instance of HTDriverModify.
   *
   * @return string
   *   Instance of self.
   */
  static public function getInstance() {
    if (!self::$instance) {
      self::$instance = new HypertableDriverDelete();
    }
    return self::$instance;
  }
}

/**
 * Hypertable Driver Insert.
 */
class HypertableDriverInsert extends HypertableDriver {
  /**
   * HypertableModify instance.
   *
   * @var string
   *   Set instance to NULL.
   */
  static protected $instance = NULL;

  /**
   * Hypertable mutator.
   *
   * @var string
   *   Set mutator.
   */
  protected $mutator;

  /**
   * Hypertable Mutator spec
   *
   * @var string
   *   Set mutateSpec.
   */
  protected $mutateSpec;

  /**
   * Hypertable cells.
   *
   * @var array
   *   Set cells as array.
   */
  protected $cells = array();

  /**
   * Set Namespace.
   *
   * @param string $ns
   *   Set namespace to operate on.
   *
   * @return string
   *   \HypertableQuery
   */
  public function setNS($ns) {
    $this->ns = $ns;
    return $this;
  }

  /**
   * Set Table.
   *
   * @param string $table
   *   Table to set.
   *
   * @return object
   *   \HypertableDriverInsert
   */
  public function setTable($table) {
    $this->table = $table;
    return $this;
  }

  /**
   * Set cell with qualifier.
   *
   * @param string $row
   *   Row to assign on set cell.
   *
   * @param string $column
   *   Column to assign on set cell.
   *
   * @param string $qualifier
   *   Qualifier to assign on set cell.
   *
   * @param string $value
   *   Value to assign on set cell.
   *
   * @return object
   *   \HypertableDriverInsert
   */
  public function setCellWithQualifier($row, $column, $qualifier, $value) {
    $key = new Hypertable_ThriftGen_Key(
      array(
        'row' => $row,
        'column_family' => $column,
        'column_qualifier' => $qualifier,
        'flag' => 255,
      )
    );
    $this->cells[] = new Hypertable_ThriftGen_Cell(
      array(
        'key' => $key,
        'value' => $value,
      )
    );
    return $this;
  }

  /**
   * Set cell.
   *
   * @param string $row
   *   Row to assign on set cell.
   *
   * @param string $column
   *   Column to assign on set cell.
   *
   * @param string $value
   *   Value to assign on set cell.
   *
   * @return object
   *   \HypertableDriverInsert
   */
  public function setCell($row, $column, $value) {
    $key = new Hypertable_ThriftGen_Key(
      array(
        'row' => $row,
        'column_family' => $column,
        'flag' => 255,
      )
    );
    $this->cells[] = new Hypertable_ThriftGen_Cell(
      array(
        'key' => $key,
        'value' => $value,
      )
    );
    return $this;
  }

  /**
   * Execute update/inseert request.
   *
   * @return object
   *   Result of update insert.
   */
  public function execute() {
    $this->ns = $this->namespace_open($this->ns);
    $insert = $this->offer_cells($this->ns, $this->table, $this->mutateSpec, $this->cells);
    $this->table = NULL;
    $this->cells = array();
    $this->mutateSpec = NULL;
    $this->namespace_close($this->ns);
    return $insert;
  }

  /**
   * Singleton get instance of HTDriverModify.
   *
   * @return string
   *   Instance of self.
   */
  static public function getInstance() {
    if (!self::$instance) {
      self::$instance = new HypertableDriverInsert();
    }
    return self::$instance;
  }
}
