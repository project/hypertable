Introduction
========================
The Hypertable module provides API access to the Hypertable Database. 
The API integrates database functionality using the Thrift Library and 
Hypertable's query language (HQL). The Hypertable module provides a library 
to build additional functionality or applications.

The hypertable_nodes module is a sample implementations 
for how to use the Hypertable API to map entities in addition 
to performing queries and displaying results from Hypertable.

Installation
========================
- Install Hypertable if you do not already have it installed 
  (http://hypertable.com/documentation/installation/).
- Copy the libraries php folder from hypertable to site/all/libraries
  so that Thrift.php lives at sites/all/libraries/hypertable/Thrift.php
  ex: cp -r /opt/hypertable/current/lib/php hypertable
- Download the Hypertable module and enable.
- Define your host, port and thrift library location in by navigation to 
  admin/config/development/hypertable.

=========================
Examples
=========================
- For an example on how to install namespaces, tables
  and manage node entities review the hypertable_nodes
  module includes as a sub module. This module also maps
  the node id and field name as the row key and column family.
  A column family is title, path, and node_object containing the
  entity as a json object.
- After enabling Hypertable Nodes you may want to set permission
  to view the node list.
  

HypertableHQL Examples
=========================
/**
 * Simple HQL.
 */
$query = HypertableQuery::getInstance()
  ->setNS('/drupal')
  ->setQuery('SELECT node_title from Nodes');
$result = $query->execute();


/**
 * Perform Select with Column, Where, Limit
 * and Offset defined.
 */
$query = HypertableSelect::getInstance()
  ->setNS('drupal_nodes')
  ->setTable('nodes')
  ->setColumn(array('node_object'))
  ->setWhere(array('timestamp', '>', '2012-12-05'))
  ->setRevs('1')
  ->setLimit($limit)
  ->setOffset($offset)
  ->setCount();
$result = $query->execute();

/**
 * Perform delete where row = something.
 */
$query = HypertableDelete::getInstance()
      ->setNS('drupal_nodes')
      ->setColumn("")
      ->setTable('nodes')
      ->setWhere(array('ROW', '=', $entity->nid));
$query->execute();

HypertableDriver Examples
=========================

/*
 * Perform simple select.
 * SQL Equivalent : SELECT * FROM node
 */
$query = HypertableDriverSelect::getInstance()
  ->setNS('/drupal')
  ->setTable("nodes");
$result = $query->execute();

/*
 * Perform simple select with row equals.
 * SQL Equivalent:
 *   SELECT news:title, news:content FROM node WHERE ROW = 42
 */	
$query = HypertableDriverSelect::getInstance()
  ->setNS('/drupal')
  ->setTable("nodes")
  ->setColumns(
    array(
      "news:title",
      "news:content"
    )
  )
  ->condition(HTDriverCondition::ROW_EQUAL, "42");
$result = $query->execute();

/*
 * Get count.
 * SQL Equivalent: SELECT COUNT(*) FROM node
 */
$query = HypertableDriverSelect::getInstance()
  ->setNS('/drupal')
  ->setTable("nodes");
$result = $query->count();		

/**
 * SQL Equivalent: SELECT * FROM node LIMIT 10 OFFSET 5
 */
$query = HypertableDriverSelect::getInstance()
  ->setNS('/drupal')
  ->setTable("nodes")
  ->setColumn('news:author')
  ->setLimit(10)
  ->setOffset(5);
$result = $query->execute();

/**
 * Get only a row.
 */
$query = HypertableDriverSelect::getInstance()
  ->setNS('/drupal')
  ->setTable("nodes")
  ->setKeyOnly(true);
$result = $query->execute();

/**
 * Insert a cell.
 */
$query = HypertableDriverInsert::getInstance()
  ->setNS('/drupal')
  ->setTable("nodes")
  ->setCell("1234", "news", "Hypertable");
$result = $query->execute();

/**
 * Insert a cell with qualifier.
 */
$query = HypertableDriverInsert::getInstance()
  ->setNS('/drupal')
  ->setTable("nodes")
  ->setCellWithQualifier("1234", "news", "title", "Hypertable")
  ->setCellWithQualifier("1234", "news", "content", "Hello world !!!!")
  ->setCellWithQualifier("1234", "news", "author", "user21");
$result = $query->execute();

/**
 * Delete a row.
 */
$query = HypertableDriverDelete::getInstance()
  ->setNS('/drupal')
  ->setTable("nodes")
  ->setRow("1234");
$result = $query->execute();
